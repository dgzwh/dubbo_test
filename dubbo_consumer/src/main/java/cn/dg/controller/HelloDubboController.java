package cn.dg.controller;


import cn.dg.service.HelloDubbo;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dg
 * @Date: 2019/12/16 8:52
 **/
@Controller
@RequestMapping("/hello")
public class HelloDubboController {

    @Reference
    private HelloDubbo dubbo;

    @RequestMapping("/say")
    @ResponseBody
    public String hello(String name) {
        String result = dubbo.hello(name);
        return result;
    }
}
