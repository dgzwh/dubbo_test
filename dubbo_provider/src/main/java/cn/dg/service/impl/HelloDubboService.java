package cn.dg.service.impl;

import cn.dg.service.HelloDubbo;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dg
 * @Date: 2019/12/16 8:51
 **/
@Service
/*
*   spring aop 底层采用动态代理 (jdk接口代理+cglib继承代理)默认使用jdk接口代理
        *   扫描包扫描的是 com.sun.proxy.$proxy54
        * */
@Transactional

public class HelloDubboService implements HelloDubbo {

    @Override
    public String hello(String str) {
        return "hello" + str;
    }
}
