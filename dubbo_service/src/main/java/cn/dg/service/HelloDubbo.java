package cn.dg.service;

/**
 * @author dg
 * @Date: 2019/12/16 8:50
 **/
public interface HelloDubbo {
    public String hello(String str);
}
